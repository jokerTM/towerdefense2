﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{

    [SerializeField]
    private GameObject CannonPrefab;

    [SerializeField]
    private GameObject MarkerPrefab;        

    [SerializeField]
    [Range(10f, 45f)]
    private float angle = 45f;              // Reduce for Faster shots due to higher velocity

    private float Dummy;

    //[SerializeField]
    //private float Range = 0.0f;

    [SerializeField]
    private int CannonBallPool = 30;         // Shots needed for object pool to start, Recommended value of 30


    [SerializeField]
    [Range(0.5f, 3f)]
    private float ShotDelay = 0.0f;         // Reload Speed Increase or decrease for faster or slower shot rate

    private float ShotMade = 0.0f;

    [SerializeField]
    public static Stack<GameObject> Cannons = new Stack<GameObject>();      // OP stack for Cannons

    [SerializeField]
    public static Stack<GameObject> Markers = new Stack<GameObject>();      // OP stack for markers


    /// <summary>
    /// Holder for shots and markers
    /// </summary>
    public GameObject CannonBallHolder;
    public GameObject MarkersHolder;


    private GameObject CannonTemp;
    private CannonBall CannonBallScript;

    private GameObject MarkersTemp;
    private Markers MarkersScript;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && Time.unscaledTime >= ShotMade + ShotDelay)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            ShotMade = Time.unscaledTime;
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                if (Markers.Count > CannonBallPool)
                {
                    MarkersTemp = Markers.Pop(); 
                }
                else
                {
                    MarkersTemp = Instantiate(MarkerPrefab);
                    MarkersTemp.transform.parent = MarkersHolder.transform;
                    Markers.Push(MarkersTemp);

                }

                MarkersScript = MarkersTemp.GetComponent<Markers>();
                MarkersScript.MarkerOnPoint(hitInfo.point);
                // Range = Vector3.Distance(this.gameObject.transform.position, hitInfo.point);
                FireCannonAtPoint(hitInfo.point);
            }
        }
    }

    #region Velocity and PointCalculation
    private void FireCannonAtPoint(Vector3 point)
    {
        Vector3 velocity = BallisticVelocity(point, angle);
        Debug.Log("Firing at " + point + " velocity " + velocity);
        if (Cannons.Count > CannonBallPool)
        {
            CannonTemp = Cannons.Pop();
            CannonBallScript = CannonTemp.GetComponent<CannonBall>();

        }
        else
        {
            CannonTemp = Instantiate(CannonPrefab);
            CannonTemp.transform.parent = CannonBallHolder.transform;
            Cannons.Push(CannonTemp);
        }

        CannonBallScript = CannonTemp.GetComponent<CannonBall>();
        CannonBallScript.FireCannonAtPoint(velocity, this.transform.position);
    }

    private Vector3 BallisticVelocity(Vector3 destination, float angle)
    {
        Vector3 dir = destination - transform.position; // get Target Direction
        float height = dir.y; // get height difference
        dir.y = 0; // retain only the horizontal difference
        float dist = dir.magnitude; // get horizontal direction
        float a = angle * Mathf.Deg2Rad; // Convert angle to radians
        dir.y = dist * Mathf.Tan(a); // set dir to the elevation angle.
        dist += height / Mathf.Tan(a); // Correction for small height differences

        // Calculate the velocity magnitude
        float velocity = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * dir.normalized; // Return a normalized vector.
    }
    #endregion

}
