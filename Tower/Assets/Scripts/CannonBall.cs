﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private Rigidbody CannonRigidBody;

    [SerializeField]
    private float CannonWaitTimeToDestroy;    // Time before getting destroyed

    [SerializeField]
    Coroutine DisableCoroutine = null;

    private void OnEnable()
    {
        DisableCoroutine = StartCoroutine(PushBackToStack());
    }

    IEnumerator PushBackToStack()
    {
        yield return new WaitForSecondsRealtime(CannonWaitTimeToDestroy);
        this.gameObject.SetActive(false);

    }

    public void FireCannonAtPoint(Vector3 velocity, Vector3 ShotPosition)
    {
        this.gameObject.SetActive(true);
        this.transform.position = ShotPosition;
        CannonRigidBody.velocity = velocity;
    }


    private void OnDisable()
    {
        Trajectory.Cannons.Push(this.gameObject);
    }

}
