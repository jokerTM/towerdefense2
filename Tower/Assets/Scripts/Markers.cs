﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Markers : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private float MarkersWaitTimeToDestroy;             //Time before getting destroyed

    [SerializeField]
    Coroutine DisableCoroutine = null;

    private void OnEnable()
    {
        DisableCoroutine = StartCoroutine(PushBackToStack());
    }

    IEnumerator PushBackToStack()
    {
        yield return new WaitForSecondsRealtime(MarkersWaitTimeToDestroy);
        this.gameObject.SetActive(false);

    }

    public void MarkerOnPoint(Vector3 Position)
    {
        this.gameObject.transform.position = Position;
    }


    private void OnDisable()
    {
        Trajectory.Markers.Push(this.gameObject);
    }
}
